'use strict'

module.exports = class Sort
	
	types = <[ bubble quick insertion selection merge counting heap radix ]>
	
	~> @original = it

	min: -> Math.min.apply null, it or @getOriginal!

	max: -> Math.max.apply null, it or @getOriginal!

	clone: -> [].concat it or @getOriginal!

	getTypes: -> return [].concat types

	getOriginal: -> @original

	bubble: (arr = @clone!) ->
		len = arr.length; i = len - 1
		while i >= 0
			j = 1; while j <= i				
				_swap arr, j - 1, j if arr[j - 1] > arr[j]
				j++
			i--
		return arr

	selection: (arr = @clone!) ->
		len = arr.length; i = 0
		while i < len 
			minIdx = i; j = i + 1
			while j < len then minIdx = j if arr[j] < arr[minIdx]; j++
			_swap arr, i, minIdx; i++
		return arr

	insertion: (arr = @clone!) ->
		len = arr.length; i = 1
		while i < len
			j = i; while j > 0 and arr[j - 1] > arr[i]
				arr[j] = arr[j - 1]; j--
			arr[j] = arr[i]; i++
		return arr

	merge: (arr = @clone!) ->
		len = arr.length
		return arr if len < 2
		mid = Math.floor len / 2
		left = arr.slice 0, mid
		right = arr.slice mid
		_merge (@merge left), @merge right

	heap: (arr = @clone!) ->
		len = arr.length; i = len - 1
		_buildMaxHeap arr		
		while i > 0
			_swap arr, 0, i
			_heapify arr, 0, --len; i--
		return arr

	radix: (arr = @clone!) ->
		k = Math.max.apply null, arr.map ( (i) -> Math.ceil (Math.log i) / Math.log 2 )
		d = 0; while d < k
			i = 0; p = 0; b = 1 << d
			n = arr.length
			while i < n
				o = arr[i]
				arr.splice p++, 0, (arr.splice i, 1)[0] if (o .&. b) is 0
				++i
			++d
		return arr

	quick: (arr = @clone!, left = 0, right = arr.length - 1) ->
		len = arr.length
		if left < right
			partitionIndex = _partition arr, pivot = right, left, right
			@quick arr, left, partitionIndex - 1
			@quick arr, partitionIndex + 1, right
		return arr

	counting: (arr = @clone!, min = @min!, max = @max! ) ->
		i = min; z = 0; count = []
		while i <= max then count[i] = 0; i++
		i = 0; while i < arr.length then count[arr[i]]++; i++
		i = min; while i <= max
			while count[i]-- > 0 then arr[z++] = i
			i++
		return arr

	_compare = (a, b) ->
		return 1 if a > b
		return -1 if a < b
		return 0

	_swap = (arr, i, j) ->
		temp = arr[i]
		arr[i] = arr[j]
		arr[j] = temp

	_insert = (arr, i, j) ->
		tmp = arr[i]
		arr.splice i, 1
		arr.splice j, 0, tmp

	_heapify = (arr, index, heapSize) ->
		largest = index; left = 2 * index + 1; right = 2 * index + 2
		largest = left if left < heapSize and arr[left] > arr[index]
		if right < heapSize and arr[right] > arr[largest] then largest = right
		if largest isnt index
			_swap arr, index, largest
			_heapify arr, largest, heapSize

	_buildMaxHeap = (arr) ->
		i = Math.floor arr.length / 2
		while i >= 0 then _heapify arr, i, arr.length; i--
		return arr

	_partition = (arr, pivot, left, right) ->
		pivotValue = arr[pivot]
		partitionIndex = left
		i = left
		while i < right
			if arr[i] < pivotValue
				_swap arr, i, partitionIndex
				partitionIndex++
			i++
		_swap arr, right, partitionIndex
		return partitionIndex

	_merge = (left, right) ->
		result = []
		l = 0; r = 0
		leftLength = left.length
		rightLength = right.length
		while l < leftLength and r < rightLength
			if left[l] < right[r] then result.push left[l++] else result.push right[r++]
		(result.concat left.slice l).concat right.slice r

