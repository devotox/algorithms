'use strict'

Sort = require \./Sort/Sort

$ ->
	return
	console.log '===================================='
	console.log 'SORTING'
	console.log '===================================='

	arr = []; for i from 1 to 15
		arr.push Math.floor ( Math.random() * 10000 ) + 1

	console.log 'Original Array', arr #= [ 22, 35, 2, 100, 400, 10000, 1, 123, 456, 892, 1445, 126, 171, 201, 223, 316, 343, 348, 432, 556, 670 ]
	
	console.log '===================================='
	
	for own x, func of ( _sort = new Sort arr).getTypes!
		if x is \counting then min = 1 if func; max = 10000
		console.log "#{func.toUpperCase!} SORT", _sort[func] arr, min, max
		console.log '===================================='
