'use strict'

module.exports = class Tree

	traversal_orders: <[ pre post bfs ]>

	binary_traversal_orders: <[ in pre post bfs ]>

	(config={}) ~>
		@set_config config
		@set_binary config
		@set_initial config

	set_initial: (config) ->
		@_private ?= {}
		@_private.data ?= {}
		@_private.data.depth = 0
		@_private.root = config.root or @

	set_config: (config) ->
		@_private ?= {}
		@_private.parent = config.parent
		@_private.data = {} <<< config.data
		@_private.children = [] <<< config.children or []

	set_binary: (config) ->
		@_private ?= {}
		@_private.left = @_private.children[0]
		@_private.right = @_private.children[1]

	getValue: -> @ggetData \value

	setValue: -> @setData \value, it

	getDepth: -> @getData('depth') or 0

	setDepth: -> @setData 'depth', @getParent!.getDepth! + 1

	getPrivate: (key) -> if key then @_private[key] else @_private

	getData: (key) -> if key then @getPrivate(\data)[key] else @getPrivate \data

	setPrivate: (key, value) -> if key and value then @_private[key] = value else @_private = key

	setData: (key, value) -> if key and value then @getPrivate(\data)[key] = value else @setPrivate \data, key

	getParent: -> @getPrivate \parent

	setParent: -> @setPrivate \parent, it

	setRoot: -> @setPrivate \root, it.getPrivate \root

	getChildren: -> @getPrivate \children

	setChildren: ->
		for own x, y of it => @addChild y
		@set_binary!

	addChild: ->
		it.setParent @; it.setRoot @; it.setDepth!
		@getPrivate('children').push it
		@set_binary!

	removeChild: ->
		@getPrivate('children').splice it, 1
		@set_binary!

	getNode: -> @

	getCurrentNode: -> @

	getRootNode: -> @getPrivate \root

	getLeftNode: -> @getPrivate \left

	getRightNode: -> @getPrivate \right

	getParentNode: -> @getPrivate \parent

	isRootNode: -> @getParentNode! in [ null, undefined ]

	isLeafNode: -> @getChildren!?.length in [ 0, null, undefined ]

	## Visiting Nodes ##
	_visit: (func, clear, limit) ->
		return if limit and @getDepth! > limit
		return if not clear and @_isVisited clear
		return unless @_setVisited!
		func? @, @getData!

	_isVisited: (clear) ->
		@_clearVisited! if clear
		@getData? 'visited'

	_setVisited: ->
		@setData? 'visited', true

	_clearVisited: ->
		@setData? 'visited', false

	size: (order='bfs', binary=false, limit) ->
		len = 0; _size = (node, data) !-> len++
		@traverse _size, false, true, order, binary, limit
		return len

	toArray: (order='bfs', binary=false, limit) ->
		arr = []; _toArray = (node, data) !-> arr.push node
		@traverse _toArray, false, true, order, binary, limit
		return arr

	toString: (order='bfs', binary=false, limit) ->
		@toArray order, binary, limit .toString!

	## Print Tree / Sub Tree ## 
	print: (order='bfs', binary=false, limit) -> 
		_print = (node, data) !->
			text=''; for i from 0 to data.depth => text += "-----------------"
			text += " depth: #{data.depth} ----- value: #{data.value}"		
			$('body').append $ '<div/>', text: text if $
			console.log text unless $
		
		@traverse _print, false, true, order, binary, limit

	find: (value, order='bfs', binary=false, limit) ->
		_find = (node, data) -> node if data.value is value		
		@traverse _find, true, true, order, binary, limit

	findAll: (value, order='bfs', binary=false, limit) ->
		arr = []; _findAll = (node, data) !-> arr.push node if data.value is value
		@traverse _findAll, true, true, order, binary, limit
		return arr

	contains: (value, order='bfs', binary=false, limit) ->
		not not @find value, order, binary, limit

	## Tree Traversal ##
	traverse: (func, search=true, clear=false, order='pre', binary=false, limit) ->
		
		return @_traverseBFS func, search, clear, binary, limit if order is 'bfs'
		return ret ?= @_visit func, clear if not binary and limit and @getDepth! > limit

		ret = null
		ret = @_visit func, clear if order is 'pre' and not binary and not ret	

		switch
			when binary 
				ret = @["_#{order}order"] func, search, clear, limit
			when order in <[ pre post ]> then for own x, node of @getChildren!
				break if ret and search; ret = node.traverse func, search, clear, order, binary, limit

		ret = @_visit func, clear if order is 'post' and not binary and not ret
		return ret

	_traverseBFS: (func, search=true, clear=false, binary=false, limit) ->
		queue = [ @ ]
		lastNodeVisited = null
		return ret if ( ret = @_visit func, clear ) and search

		while queue.length
			lastNodeVisited = queue.shift!
			break if limit and lastNodeVisited.getDepth! > limit
			break if ret and search; for own x, node of lastNodeVisited.getChildren!
				break if ( ret = node._visit func, clear ) and search
				break if binary and x >= 1
				queue.push node

		return ret

	# Iterative #
	_inorder: (func, search=true, clear=false, limit) ->
		stack = [ ]; node = @
		
		while stack.length or node
			break if ret and search
			if node
				stack.push node
				node = node.getLeftNode!
			else 
				node = stack.pop!
				ret = node._visit func, clear, limit
				node = node.getRightNode!
		return ret

	_preorder: (func, search=true, clear=false, limit) ->
		stack = [ ]; node = @
		
		while stack.length or node
			break if ret and search
			break if limit and node.getDepth! > limit
			if node
				ret = node._visit func, clear, limit
				stack.push rightNode if rightNode = node.getRightNode!
				node = node.getLeftNode!
			else node = stack.pop!
		return ret

	_postorder: (func, search=true, clear=false, limit) ->
		stack = [ ]; node = @
		lastNodeVisited = null

		while stack.length or node
			break if ret and search
			if node
				stack.push node
				node = node.getLeftNode!
			else 
				peekNode = stack[ stack.length - 1 ]
				rightNode = peekNode.getRightNode!

				if rightNode and rightNode isnt lastNodeVisited
					node = rightNode
				else 
					ret = peekNode._visit func, clear, limit
					lastNodeVisited = stack.pop!		
		return ret

	/*
	# Recursive #
	_inorder: (func, search=true, clear=false, limit) ->
		return if @_isVisited node; @_setVisited node
		ret = @getLeftNode!?._inorder func, search, clear, limit
		ret = @_visit func, clear, limit if not search or not ret
		ret = @getRightNode!?._inorder func, search, clear, limit if not search or not ret
		return ret

	_preorder: (func, node=@, search=true, clear=false, limit) ->
		return if @_isVisited!; @_setVisited!; ret = @_visit func, clear, limit
		ret = @getLeftNode!?._preorder func, search, clear, limit if not search or not ret
		ret = @getRightNode!?._preorder func, search, clear, limit if not search or not ret
		return ret

	_postorder: (func, search=true, clear=false, limit) ->
		return if @_isVisited!; @_setVisited!
		ret = @getLeftNode!?_postorder func, search, clear, limit if not search or not ret
		ret = @getRightNode!?_postorder func, search, clear, limit if not search or not ret
		ret = @_visit func, clear, limit if not search or not ret
		return ret
	*/
