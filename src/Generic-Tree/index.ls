'use strict'

Tree = require \./Generic-Tree/Tree
Sort = require \./Sort/Sort

$ ->
	return
	console.log '===================================='
	console.log 'GENERIC TREE'
	console.log '===================================='
	console.log 'Tree', tree = new Tree data: value: 0
	console.log 'Tree Size', tree.size!

	console.log '===================================='

	console.log 'Root Node', root = tree.getRootNode!

	console.log '===================================='

	console.log 'Is Root Node:', root.isRootNode!
	console.log 'Is Leaf Node:', root.isLeafNode!

	console.log '===================================='
	console.log 'Adding Data to Tree'

	for i from 1 to 5
		node = new Tree data: value: i
		tree.addChild node

		for v from 6 to 10
			node2 = new Tree data: value: v
			node.addChild node2

			for a from 11 to 15
				node3 = new Tree data: value: a
				node2.addChild node3

	console.log '===================================='
	
	console.log 'Is Root Node:', root.isRootNode!
	console.log 'Is Leaf Node:', root.isLeafNode!

	console.log '===================================='

	if $ then for own x, y of tree.binary_traversal_orders
		$('body').append $('<h1/>', text: "#{y.toUpperCase!}-Order")
		$('body').append $('<h2/>', text: "Binary")

		tree.print y, true

		$('body').append $('<hr/>')
		$('body').append $('<h2/>', text: "Full")
		$('body').append $('<hr/>')
		
		tree.print y

		$('body').append $('<hr/>')

	console.log '===================================='
	console.log 'BFS Search'

	console.log tree.find 8, 'bfs'

	console.log '===================================='
	console.log 'PRE DFS Search'

	console.log tree.find 8, 'pre'

	console.log '===================================='
	console.log 'POST DFS Search'

	console.log tree.find 8, 'post'

	console.log '===================================='
	console.log 'Size', tree.size!
	console.log 'Tree Array', tree.toArray!
	console.log 'Find All 6', tree.findAll 6
	console.log 'Contains 12', tree.contains 12
