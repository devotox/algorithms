'use strict'


module.exports = class SkipList

	infinity = Infinity

	(config={}) ~>
		@set_config config
		@set_initial config

	set_initial: (config) ->
		@_private.bottom = @newNode 0
		@_private.tail = @newNode infinity

		@_private.bottom.setDown @_private.bottom
		@_private.bottom.setRight @_private.bottom

		@_private.tail.setRight @_private.tail
		@_private.header = @newNode infinity, @_private.tail, @_private.bottom

	set_config: (config) ->
		@_private ?= {}
		@_private.data = {} <<< config.data

	getPrivate: (key) -> if key then @_private[key] else @_private

	getData: (key) -> if key then @getPrivate('data')[key] else @getPrivate \data

	setPrivate: (key, value) -> if key and value then @_private[key] = value else @_private = key

	setData: (key, value) -> if key and value then @getPrivate(\data)[key] = value else @setPrivate \data, key

	newNode: (value, right, down) -> new SkipNode down: down, right: right, data: value: value

	getTail: -> @getPrivate \tail

	getHeader: -> @getPrivate \header

	getBottom: -> @getPrivate \bottom

	setTail: -> @setPrivate \tail, it

	setHeader: -> @setPrivate \header, it

	setBottom: -> @setPrivate \bottom, it

	empty: -> 
		header = @getHeader!
		header.setRight @getTail!
		header.setDown @getBottom!

	isEmpty: -> 
		header = @getHeader!
		( header.getRight! is @getTail! ) and ( header.getDown! is @getBottom! )

	getValue: -> if it is @getBottom! then 0 else it.getValue!

	insert: !->
		current = @getHeader!
		( bottom = @getBottom! ).setValue it

		while current isnt bottom
			while current.getValue! < it then current = current.getRight!

			/*  If gap size is 3 or at bottom level and must insert, then promote middle element */
			if current.getDown!?.getRight!?.getRight!?.getValue! < current.getValue!
				current.setRight @newNode current.getValue!, current.getRight!, current.getDown!?.getRight!?.getRight!
				current.setValue current.getDown!?.getRight!?.getValue!
			else current = current.getDown!
		
		@setHeader @newNode infinity, @getTail!, @getHeader! if @getHeader!.getRight! isnt @getTail!

	find: ->
		current = @getHeader!

		while current.getDown! isnt @getBottom!
			current = current.getDown!
		
		while current.getRight! isnt @getTail!
			( ret = current; break ) if current.getValue! is it
			
			node = null; for own x, y of current.getSkipPoints!
				(current = node; break ) if node = current.getSkipPoint(y) and node.getValue! <= num				
			break if node; current = current.getRight!
		return ret

	print: -> 
		text = ""
		current = @getHeader!

		while current.getDown! isnt @getBottom!
			current = current.getDown!
		while current.getRight! isnt @getTail!
			text += current.getValue! + " "
			current = current.getRight!
		console.log "SkipList:", text

	toArray: ->
		arr = []; current = @getHeader!

		while current.getDown! isnt @getBottom!
			current = current.getDown!
		while current.getRight! isnt @getTail!
			if it then arr.push current.getValue! else arr.push current
			current = current.getRight!
		return arr

class SkipNode

	directions = <[ down right ]>

	skip_points = <[ 75 50 25 10 ]>

	capitalize = (str) -> str.charAt 0 .toUpperCase! + str.slice 1

	(config={}) ~>
		@set_config config

	set_config: (config) ->
		@_private ?= {}
		@_private.data = {} <<< config.data
		for own x, y of directions then @_private[y] = config[y]
	
	getValue: -> @getData \value

	setValue: -> @setData \value, it

	getPrivate: (key) -> if key then @_private[key] else @_private

	setPrivate: (key, value) -> if key and value then @_private[key] = value else @_private = key

	getData: (key) -> if key then @getPrivate(\data)[key] else @getPrivate \data

	setData: (key, value) -> if key and value then @getPrivate(\data)[key] = value else @setPrivate \data, key

	getUp: -> @getPrivate \up

	getDown: -> @getPrivate \down

	getLeft: -> @getPrivate \left

	getRight: -> @getPrivate \right

	setUp: -> @setPrivate \up, it

	setDown: -> @setPrivate \down, it

	setLeft: -> @setPrivate \left, it

	setRight: -> @setPrivate \right, it

	getSkipPoint: -> @getPrivate "lane_#{it}"

	setSkipPoint: (p, v) -> @setPrivate "lane_#{p}", v

	getSkipPoints: -> [].concat skip_points

	getAll: -> 
		ret = {}; for own x, y of directions
			ret[y] = @["get#{capitalize y}"]!
		return ret

	setAll: -> for own x, y of directions then @setPrivate y, it?[y] or it




