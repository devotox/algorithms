'use strict'

SkipList = require \./Skip-List/SkipList

$ ->
	console.log '===================================='
	console.log 'SKIP LIST'
	console.log '===================================='
	console.log 'List', list = new SkipList!

	console.log '===================================='

	arr = []; for i from 1 to 15
		arr.push Math.floor ( Math.random() * 10000 ) + 1

	console.log 'Original Array', arr #= [ 22, 35, 2, 100, 400, 10000, 1, 123, 456, 892, 1445, 126, 171, 201, 223, 316, 343, 348, 432, 556, 670 ]

	console.log '===================================='

	for own x, y of arr
		list.insert y; list.print!
		console.log '===================================='

	console.log 'List Array', list.toArray!
	console.log 'List Value Array', list.toArray true
	console.log "Find Node with Value: #{arr[3]}", list.find arr[3]
